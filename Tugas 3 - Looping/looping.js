// soal_1 (loop using `while`)
let idx = 2;

console.log("LOOPING PERTAMA");
while (idx <= 20) {
    console.log(`${idx} - I love coding`);
    idx += 2;
}

console.log("LOOPING KEDUA");
while (idx >= 2) {
    console.log(`${idx} - I will become a mobile developer`);
    idx -= 2;
}




// soal_2 (loop using `for`)
for (idx = 1; idx <= 20; idx++) {
    if (idx % 2 === 0) {
        console.log(`${idx} - Berkualitas`);
    } else {
        if (idx % 3 === 0) {
            console.log(`${idx} - I Love Coding`);
        } else {
            console.log(`${idx} - Santai`);
        }
    }
}




// soal_3 (membuat persegi panjang)
let col = 8;
let row = 4;
let stringLine;

for (let rowIdx = 1; rowIdx <= row; rowIdx++) {
    stringLine = "";
    for (let colIdx = 1; colIdx <= col; colIdx++) {
        stringLine += "#";
    }
    console.log(stringLine);
}




// soal_4 (membuat tangga)
row = 7;

for (let rowIdx = 1; rowIdx <= row; rowIdx++) {
    stringLine = "";
    for (let colIdx = 1; colIdx <= rowIdx; colIdx++) {
        stringLine += "#";
    }
    console.log(stringLine);
}




// soal_5 (membuat papan catur)
let fieldSize = 8;
let element = [' ', '#'];

for (let rowIdx = 1; rowIdx <= fieldSize; rowIdx++) {
    stringLine = "";

    if (rowIdx % 2 === 0) {
        for (let colIdx = 1; colIdx <= fieldSize; colIdx++) {
            stringLine += element[colIdx % 2];
        }
    } else {
        for (let colIdx = 1; colIdx <= fieldSize; colIdx++) {
            stringLine += element[(colIdx - 1) % 2];
        }
    }

    console.log(stringLine);
}
