import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import LoginScreen from './LoginScreen';
import AboutScreen from './AboutScreen';

export default function App() {
  return (
    // please uncomment one of the following to see either the Login or About Screen

    // <LoginScreen />
    <AboutScreen />
  );
}