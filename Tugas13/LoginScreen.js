import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

class LoginInputBox extends React.Component {
    render() {
        return (
            <View style={styles.loginInputBox}>
                <Icon style={styles.loginInputIcon} name={this.props.name} size={20} color="#8A8A8A"/>
                <TextInput
                    style={styles.loginInputText}
                    placeholder={this.props.placeholder}
                    underlineColorAndroid="transparent"
                />
            </View>
        )
    }
}

class RememberCheckBox extends React.Component {
    render() {
        return (
            <View style={{flexDirection: 'row'}}>
                <Icon style={{marginRight: 5}} name="check-box-outline-blank" size={20} color="#25A9E0"/>
                <Text style={{color: '#8A8A8A'}}>Remember Me</Text>
            </View>
        )
    }
}

class LoginButton extends React.Component {
    render() {
        return (
            <TouchableOpacity>
                <View style={styles.loginButtonBox}>
                    <Text style={{color: 'white'}}>LOGIN</Text>
                </View>
            </TouchableOpacity>
        )
    }

}

class LoginComponent extends React.Component {
    render() {
        return (
            <View style={styles.loginContainer}>
                <LoginInputBox name="email" placeholder="Email"/>
                <LoginInputBox name="lock" placeholder="Password"/>
                <RememberCheckBox />
                <LoginButton />
            </View>
        )
    }
}

class AlternativeLoginComponent extends React.Component {
    render() {
        return (
            <View style={styles.alternativeLoginContainer}>
                <TouchableOpacity>
                    <Image source={require('./images/login_google.svg')} style={{ width: 120, height: 22 }} />       
                </TouchableOpacity>
                <TouchableOpacity>
                    <Image source={require('./images/login_github.svg')} style={{ width: 120, height: 22 }} />
                </TouchableOpacity>
            </View>
        );
    }
}

// Main Page Container
export default class LoginScreen extends React.Component {
    render() {
        return (
            <View style={styles.appContainer}>
                <Image 
                    source={{ uri: 'https://sanbercode.com/assets/img/identity/logo-horizontal.svg' }} 
                    style={styles.appLogo} 
                />
                <LoginComponent/>
                <Text style={{color: '#8A8A8A'}}>Or login with</Text>
                <AlternativeLoginComponent />
            </View>
        )
    }
}

// Stylesheet
const styles = StyleSheet.create({
    appContainer: {
        flex: 1,
        backgroundColor: '#fff',
        paddingVertical: '15vh',
        alignContent: 'center',
        textAlign: 'center',
    },
    appLogo: {
        height: '20%',
        resizeMode: 'contain',
        marginBottom: 15,
    },
    loginContainer: {
        paddingHorizontal: '14vw',
        marginBottom: 20,
    },
    loginInputBox: {
        flex: 1,
        borderRadius: 5,
        paddingLeft: 10,
        paddingVertical: 10,
        marginBottom: 10,
        flexDirection: 'row',
        backgroundColor: '#D0D0D0',
    },
    loginInputText: {
        height: 25,
        marginLeft: 10,
        color: '#8A8A8A',
        fontSize: 16
    },
    loginInputIcon: {
        marginTop: 3,
    },
    loginButtonBox: {
        backgroundColor: '#25A9E0',
        paddingVertical: 10,
        borderRadius: 5,
        marginTop: 20
    },
    alternativeLoginContainer: {
        flexDirection:'row',
        marginTop: 30,
        justifyContent: 'space-between',
        paddingHorizontal: '14vw'
    }
  });