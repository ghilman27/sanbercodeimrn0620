/*
    Soal No. 2 (Promise Baca Buku)
    Setelah no.1 berhasil, implementasikan function readBooks yang menggunakan callback di atas namun sekarang menggunakan Promise. 
*/

// Masih di folder yang sama dengan promise.js, buatlah sebuah file dengan nama index2.js. Tuliskan code sebagai berikut

var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
// Lakukan hal yang sama dengan soal no.1, habiskan waktu selama 10000 ms (10 detik) untuk membaca semua buku dalam array books.!

const accepted = (time) => {
    i++;
    if (i < books.length) {
        readBooksPromise(time, books[i])
        .then(accepted)
        .catch(rejected)
    } else {
        console.log("Tidak ada buku lagi")
    }
}

const rejected = (time) => {
    console.log(`Sisa waktu : ${time}`);
}

let i = 0;
let time = 10000;
readBooksPromise(time, books[i])
    .then(accepted)
    .catch(rejected)