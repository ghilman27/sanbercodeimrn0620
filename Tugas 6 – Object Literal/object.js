/*
    Soal No. 1 (Array to Object)
    Buatlah function dengan nama `arrayToObject()` yang menerima sebuah parameter berupa array multidimensi. Dalam array tersebut berisi value berupa First Name, Last Name, Gender, dan Birthyear. Data di dalam array dimensi tersebut ingin kita ubah ke dalam bentuk Object dengan key bernama : firstName, lastName, gender, dan age. Untuk key age ambillah selisih tahun yang ditulis di data dengan tahun sekarang. Jika tahun tidak terdefinisi atau tahunnya lebih besar dibandingkan dengan tahun sekarang maka kembalikan nilai : “Invalid birth year”.

    Contoh: jika input nya adalah 
            [["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985]]

    maka outputnya di console seperti berikut :
            1. Abduh Muhamad : { firstName: "Abduh", lastName: "Muhamad", gender: "male", age: 28}
            2. Ahmad Taufik : { firstName: "Ahmad", lastName: "Taufik", gender: "male", age: 35} 

    Untuk mendapatkan tahun sekarang secara otomatis bisa gunakan Class `Date` dari Javascript.
            var now = new Date()
            var thisYear = now.getFullYear() // 2020 (tahun sekarang)
*/

const arrayToObject = (array) => {
    if (!array || array.length === 0) return "";

    const ageIdentifier = (birthYear) => {
        let now = new Date();
        if (birthYear < now.getFullYear()) return now.getFullYear() - birthYear;
        else return "Invalid Birth Year";
    }
    
    let objOutput = {};
    for (data of array) {
        objOutput[`${data[0]} ${data[1]}`] =  {
            firstName: data[0],
            lastName: data[1],
            gender: data[2],
            age: ageIdentifier(data[3]),
        }
    }

    return objOutput;
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ];

console.log(arrayToObject(people));
console.log(arrayToObject(people2));
console.log(arrayToObject([]));




/*
    Soal No. 2 (Shopping Time)
    Problem

    Diberikan sebuah function `shoppingTime(memberId, money)` yang menerima dua parameter berupa string dan number. 
    Parameter pertama merupakan `memberId` dan parameter ke-2 merupakan value uang (`money`) yang dibawa oleh member tersebut.

    Toko X sedang melakukan SALE untuk beberapa barang, yaitu:

    Sepatu brand Stacattu seharga 1500000
    Baju brand Zoro seharga 500000
    Baju brand H&N seharga 250000
    Sweater brand Uniklooh seharga 175000
    Casing Handphone seharga 50000
    Buatlah function yang akan mengembalikan sebuah object dimana object tersebut berisikan informasi memberId, money, listPurchased dan changeMoney.

    Jika memberId kosong maka tampilkan “Mohon maaf, toko X hanya berlaku untuk member saja”
    Jika uang yang dimiliki kurang dari 50000 maka tampilkan “Mohon maaf, uang tidak cukup”
    Member yang berbelanja di toko X akan membeli barang yang paling mahal terlebih dahulu dan akan membeli barang-barang yang sedang SALE masing-masing 1 jika uang yang dimilikinya masih cukup.
    Contoh jika inputan memberId: ‘324193hDew2’ dan money: 700000

    maka output:

    { memberId: ‘324193hDew2’, money: 700000, listPurchased: [ ‘Baju Zoro’, ‘Sweater Uniklooh’ ], changeMoney: 25000 }
*/

const shoppingTime = (memberId, money) => {
    if (!memberId || memberId.length === 0 ) return "Mohon maaf, toko X hanya berlaku untuk member saja";
    if (money < 50000) return "Mohon maaf, uang tidak cukup";

    const unorderedItems = {
        "Sepatu Stacattu" : 1500000,
        "Baju Zoro" : 500000,
        "Baju H&N" : 250000,
        "Sweater Uniklooh" : 175000,
        "Casing Handphone" : 50000,
    }
    
    // because I have doubt whether object in JS is ordered based on their declaration or not, better do this just to be safe
    const orderedItems = Object.keys(unorderedItems).sort((item1, item2) => unorderedItems[item2] - unorderedItems[item1])

    let changeMoney = money;
    let listPurchased = [];
    for (item of orderedItems) {
        if (unorderedItems[item] <= changeMoney) {
            changeMoney -= unorderedItems[item];
            listPurchased.push(item);
        }
    }

    return {
        memberId: memberId,
        money: money,
        listPurchased: listPurchased,
        changeMoney: changeMoney,
    }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));

console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja




/*
    Soal No. 3 (Naik Angkot)
    Problem

    Diberikan function naikAngkot(listPenumpang) yang akan menerima satu parameter berupa array dua dimensi. Function akan me-return array of object.

    Diberikan sebuah rute, dari A – F. Penumpang diwajibkan membayar Rp2000 setiap melewati satu rute.

    Contoh: input: [ [‘Dimitri’, ‘B’, ‘F’] ] output: [{ penumpang: ‘Dimitri’, naikDari: ‘B’, tujuan: ‘F’, bayar: 8000 }]
*/

function naikAngkot(arrPenumpang) {
    const rute = ['A', 'B', 'C', 'D', 'E', 'F'];

    //your code here
    if (!arrPenumpang || arrPenumpang.length === 0) return [];

    let bayar;
    let output = [];
    for (data of arrPenumpang) {
        bayar = (rute.indexOf(data[2]) - rute.indexOf(data[1])) * 2000;
        output.push(
            {
                penumpang: data[0],
                naikDari: data[1],
                tujuan: data[2],
                bayar: bayar,
            }
        )
    }

    return output;
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]