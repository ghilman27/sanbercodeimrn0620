// ----------- werewolf ----------- 
let nama = "John"
let peran = ""

const wereWolf = (nama, peran) => {
    if (nama.length === 0 && peran.length === 0) return 'nama harus diisi!';
    if (nama.length !== 0 && peran.length === 0) return `Halo ${nama}, Pilih peranmu untuk memulai game!`;

    if (peran.toLowerCase() === 'penyihir') return `Selamat datang di Dunia Werewolf, ${nama}\nHalo Penyihir ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`;
    if (peran.toLowerCase() === 'guard') return `Selamat datang di Dunia Werewolf, ${nama}\nHalo Guard ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`;
    if (peran.toLowerCase() === 'werewolf') return `Selamat datang di Dunia Werewolf, ${nama}\nHalo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!`;
}

console.log(wereWolf(nama, peran))




// ----------- switch case ----------- 
var tanggal = 21; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 1; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1945; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)


const switchCase = (tanggal, bulan, tahun) => {
    let namaBulan;
    switch (bulan) {
        case 1:
          namaBulan = "Januari";
          break;
        case 2:
          namaBulan = "Februari";
          break;
        case 3:
          namaBulan = "Maret";
          break;
        case 4:
          namaBulan = "April";
          break;
        case 5:
          namaBulan = "Mei";
          break;
        case 6:
          namaBulan = "Juni";
          break;
        case 7:
          namaBulan = "Juli";
          break;
        case 8:
          namaBulan = "Agustus";
          break;
        case 9:
          namaBulan = "September";
          break;
        case 10:
          namaBulan = "Oktober";
          break;
        case 11:
          namaBulan = "November";
          break;
        case 12:
          namaBulan = "December";
          break;
    }

    return `${tanggal} ${namaBulan} ${tahun}`;
}

console.log(switchCase(tanggal, bulan, tahun));
