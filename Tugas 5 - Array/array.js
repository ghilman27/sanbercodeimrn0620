// ---------------- SOAL 1 -----------------
// Code di sini
const range = (startNum, finishNum) => {
    // input check
    if (!(startNum && finishNum)) return -1;
    if (startNum == finishNum) return [startNum];

    // order check
    let step = 1;
    if (startNum > finishNum) step = -1;

    // array iteration
    let array = [];
    for (let i=startNum; i!=finishNum; i+=step) {
        array.push(i);
    }

    // finish and return
    array.push(finishNum);
    return array;
}
 
console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11,18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1 




// ---------------- SOAL 2 -----------------
// Code di sini
const rangeWithStep = (startNum, finishNum, step = 1) => {
    // input check
    if (!(startNum && finishNum)) return -1;
    if (startNum == finishNum) return [startNum];

    let array = [];
    if (startNum > finishNum) {
        for (let i=startNum; i>=finishNum; i-=step) {
            array.push(i);
        }
    } else {
        for (let i=startNum; i<=finishNum; i+=step) {
            array.push(i);
        }
    }

    return array;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 




// ---------------- SOAL 3 -----------------
// Code di sini
const sum = (startNum, finishNum, step = 1) => {
    // input check
    if (!(startNum || finishNum)) return 0;
    if (startNum && !finishNum) return startNum;
    if (!startNum && finishNum) return finishNum;

    const array = rangeWithStep(startNum, finishNum, step);

    let total = 0;
    for (let value of array) {
        total += value;
    }

    return total;
}

console.log(sum(1,10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15,10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0 




// ---------------- SOAL 4 -----------------
//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];
            
const dataHandling = (input) => {
    // input check
    if (!input && !input.length) {
        throw new Error("Input is not an array or is empty, please check again");
    }

    // object mode
    input.forEach(data => {
        let object = {
            "Nomor ID": data[0],
            "Nama Lengkap": data[1],
            "TTL": `${data[2]} ${data[3]}`,
            "Hobi": data[4],
        }
        console.log(object);
    });
} 

dataHandling(input);




// ---------------- SOAL 5 -----------------
// Code di sini
const balikKata = (aString) => {
    if (typeof(aString) != "string") throw new Error('input is not a string');

    let reversedWord = '';

    for (var i = aString.length - 1; i >= 0; i--) {
        reversedWord += aString[i];
    }
    return reversedWord;
}
 
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 




// ---------------- SOAL 6 -----------------
const extractMonth = (bulan) => {
    let namaBulan;
    bulan = parseInt(bulan);

    switch (bulan) {
        case 1:
          namaBulan = "Januari";
          break;
        case 2:
          namaBulan = "Februari";
          break;
        case 3:
          namaBulan = "Maret";
          break;
        case 4:
          namaBulan = "April";
          break;
        case 5:
          namaBulan = "Mei";
          break;
        case 6:
          namaBulan = "Juni";
          break;
        case 7:
          namaBulan = "Juli";
          break;
        case 8:
          namaBulan = "Agustus";
          break;
        case 9:
          namaBulan = "September";
          break;
        case 10:
          namaBulan = "Oktober";
          break;
        case 11:
          namaBulan = "November";
          break;
        case 12:
          namaBulan = "December";
          break;
    }

    return namaBulan;
}

const dataHandling2 = (input) => {
    // Gunakan fungsi splice untuk memodifikasi variabel
    input.splice(1, 1, `${input[1]} Elsharawy`);
    input.splice(2, 1, `Provinsi ${input[2]}`);
    input.splice(4, 1, "Pria");
    input.splice(5, 0, "SMA Internasional Metro");

    // Lalu console.log array yang baru
    console.log(input);

    // Berdasarkan elemen yang berisikan tanggal/bulan/tahun (elemen ke-4), ambil angka bulan dan console.log nama bulan sesuai dengan angka tersebut.
    console.log(extractMonth(input[3].split('/')[1]));

    // Pada array hasil split dari tanggal/bulan/tahun, lakukan sorting secara descending dan console.log array yang sudah di-sort.
    console.log(input[3].split('/').sort((a,b) => b - a));

    // Masih pada array hasil split dari elemen tanggal/bulan/tahun, gabungkan semua elemen menggunakan join dan pisahkan dengan karakter strip (-) lalu console.log hasilnya.
    console.log(input[3].split('/').join("-"));

    // Nama (elemen ke-2), harus dibatasi sebanyak 15 karakter saja. Gunakan slice untuk menghapus kelebihan karakter dan console.log nama yang sudah di-slice, sebelum di-slice pastikan Nama (elemen ke-2) sudah dalam bentuk String agar bisa di-slice.
    console.log(input[1].slice(0, 15));
}


var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 