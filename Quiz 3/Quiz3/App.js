import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import 'react-native-gesture-handler';

import LoginScreen from './LoginScreen';
import HomeScreen from './HomeScreen';

const Stack = createStackNavigator();
// const PriceContext = React.createContext();

export default class App extends React.Component {
	render() {
		return (
			// <PriceContext.Provider value={}>
				<NavigationContainer>
					<Stack.Navigator initialRouteName='Login'>
						<Stack.Screen
							name='Login'
							component={LoginScreen}
							options={{ headerShown: false }}
						/>
						<Stack.Screen
							name='Home'
							component={HomeScreen}
							options={{ headerTitle: 'Daftar Barang' }}
						/>
					</Stack.Navigator>
				</NavigationContainer>
			// </PriceContext.Provider>
		);
	}
}
